package main.java.com.virtualhand.server.responsemessages;

public final class ResponseMessages {

	public static final String SUCCESS = "SUCCESS";
	public static final String INVALID_LONG = "INVALID LONG";
	public static final String INVALID_INTEGER = "INVALID INT";
	public static final String NON_UNIQUE_USERNAME = "YOUR USERNAME IS NOT UNIQUE";
	public static final String NON_UNIQUE_CLASS_SCHOOL_PAIR = "THERE IS ANOTHER CLASS WITH THAT NAME IN YOUR SCHOOL";
	public static final String USERNAME_NOT_SENT = "USERNAME IS REQUIRED";
	public static final String PASSWORD_NOT_SENT = "PASSWORD REQUIRED";
	public static final String VARIABLE_NOT_SENT = "REQUIRED VARIABLE HAS NOT BE SENT";
	public static final String INVALID_ID = "THAT ID IS INVALID";
	public static final String INVALID_CLASS_ID = "THAT CLASS ID IS INVALID";
	public static final String UNAUTHORIZED_TEACHER = "THIS TEACHER IS NOT ENROLLED IN THAT CLASS";
	public static final String INVALID_HANDSTATE = "HANDSTATE MUST BE 1 or 2";
	public static final String INVALID_STUDENT = "THAT STUDENT IS NOT IN THAT CLASS";
    public static final String TOO_MANY_CLASSES = "TOO MANY CLASSES";
    public static final String INVALID_KEY = "INVALID KEY";
	public static final String OTHER_FAILURE = "UNSPECIFIED FAILURE";

}
