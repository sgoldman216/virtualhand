/***********************************************************
 * 
 * Copyright (c) 2007-2014 www.cropster.com
 * 
 * Created: Jul 15, 2014
 * 
 * Author: martin/cropster
 * 
 ***********************************************************/
package main.java.com.virtualhand.server.security;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * Logs the username, User-Agent and {@link AuthenticationException} for
 * debugging reasons for all requests made via the REST API.
 */
public class RestAuthenticationFilter extends BasicAuthenticationFilter
{
    private static final Logger log = LoggerFactory
            .getLogger(RestAuthenticationFilter.class);

    public RestAuthenticationFilter(
            AuthenticationManager authenticationManager,
            AuthenticationEntryPoint authenticationEntryPoint)
    {
        super(authenticationManager, authenticationEntryPoint);
    }

    /* (non-Javadoc)
     * @see org.springframework.security.web.authentication.www.BasicAuthenticationFilter#onSuccessfulAuthentication(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.Authentication)
     */
    @Override
    protected void onSuccessfulAuthentication(HttpServletRequest request,
            HttpServletResponse response, Authentication authResult)
            throws IOException
    {
        String username = authResult.getName();
        String userAgent = request.getHeader("User-Agent");
        log.info(String.format("[%s] %s", username, userAgent));

        super.onSuccessfulAuthentication(request, response, authResult);
    }

    /* (non-Javadoc)
     * @see org.springframework.security.web.authentication.www.BasicAuthenticationFilter#onUnsuccessfulAuthentication(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
     */
    @Override
    protected void onUnsuccessfulAuthentication(HttpServletRequest request,
            HttpServletResponse response, AuthenticationException failed)
            throws IOException
    {
        String username = "?";
        if ((SecurityContextHolder.getContext() != null)
                && (SecurityContextHolder.getContext().getAuthentication() != null))
        {
            username = SecurityContextHolder.getContext()
                    .getAuthentication().getName();
        }
        String userAgent = request.getHeader("User-Agent");
        log.warn(String.format("[%s] %s - %s", username, userAgent, failed));

        super.onUnsuccessfulAuthentication(request, response, failed);
    }
}
