package main.java.com.virtualhand.server.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Hex;

public class TokenUtils {

	/**
	 * Random UUID that is used to compute the tokens signature.
	 */
	private static final String SALT = "DDF3188E-C338-44D8-BEE3-BE367885A0AA";

	/**
	 * Returns a new authentication token from the passed {@link UserDetails}.
	 * It contains the username, expiration date (1 hour after its generation)
	 * and a signature to validate the token. The values are separated by colons
	 * (:).
	 * 
	 * @param userDetails
	 *            for which the token should be generated.
	 * @return a token in the form of <b>
	 *         <tt>USERNAME:EXPIRATION_DATE:SIGNATURE</tt></b> (e.g
	 *         <tt>user:1405417645229:8f4d77d5139c719b5363571b766e41ba</tt>).
	 */
	public static String createToken(UserDetails userDetails) {

		/* Token expires in one hour */
		long expires = System.currentTimeMillis() + 1000L * 60 * 60;

		StringBuilder tokenBuilder = new StringBuilder();
		tokenBuilder.append(userDetails.getUsername());
		tokenBuilder.append(":");
		tokenBuilder.append(expires);
		tokenBuilder.append(":");
		tokenBuilder.append(TokenUtils.computeSignature(userDetails, expires));

		return tokenBuilder.toString();
	}

	/**
	 * Computes the token signature based on the {@link UserDetails} username,
	 * expiration time and a salt. The combined string is MD5 encoded.
	 * 
	 * @param userDetails
	 *            from which the username is taken.
	 * @param expires
	 *            the date when the token expiers.
	 * @throws @{link {@link IllegalStateException} if no MD5 algorithem is
	 *         available.
	 * @return the token signature as MD5 hash.
	 */
	private static String computeSignature(UserDetails userDetails, long expires) {

		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(userDetails.getUsername());
		signatureBuilder.append(":");
		signatureBuilder.append(expires);
		signatureBuilder.append(":");
		signatureBuilder.append(userDetails.getPassword());
		signatureBuilder.append(":");
		signatureBuilder.append(TokenUtils.SALT);

		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("No MD5 algorithm available!");
		}

		return new String(Hex.encode(digest.digest(signatureBuilder.toString()
				.getBytes())));
	}

	/**
	 * Returns the username of the given token.
	 * 
	 * @param authToken
	 *            from which the username should be returned.
	 * @return the username or null (if it's not a valid token)
	 */
	public static String getUserNameFromToken(String authToken) {

		if (authToken == null) {
			return null;
		}

		String[] parts = authToken.split(":");
		return parts.length == 3 ? parts[0] : null;
	}

	/**
	 * Checks if the authentication token is valid. Validity is based on the
	 * expiration time (not yet expired) and a matching token signature.
	 * 
	 * @param authToken
	 *            that should be validated.
	 * @param userDetails
	 *            of the user this token belongs to.
	 * @return true if it's not expired and has a matching signature, otherwise
	 *         false
	 */
	public static boolean validateToken(String authToken,
			UserDetails userDetails) {

		String[] parts = authToken.split(":");
		long expires = Long.parseLong(parts[1]);
		String signature = parts[2];

		if (expires < System.currentTimeMillis()) {
			return false;
		}

		return signature.equals(TokenUtils.computeSignature(userDetails,
				expires));
	}
}