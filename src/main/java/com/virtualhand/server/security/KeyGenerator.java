package main.java.com.virtualhand.server.security;

import java.math.BigInteger;
import java.security.SecureRandom;

public class KeyGenerator {

	private SecureRandom random;
	
	public static void main(String[] args){
		System.out.println(new KeyGenerator().getRandomKey());
	}

	public KeyGenerator() {
		random = new SecureRandom();
	}

	public String getRandomKey() {
		return new BigInteger(130, random).toString(32);
	}
}
