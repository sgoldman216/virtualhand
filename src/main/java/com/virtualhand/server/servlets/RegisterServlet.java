package main.java.com.virtualhand.server.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;

public class RegisterServlet implements HttpRequestHandler {

	@Override
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.getWriter()
				.println(
						"<html>"
								+ "<body>"
								+ " <form action=\"registerpost\" method=\"POST\">"
								+ " Username: <input type=\"text\" name=\"username\"><br />"
								+ "Password: <input type=\"password\" name=\"password\" /><br />"
								+ "API Key: <input type=\"text\" name=\"apikey\">"
								+ "<input type=\"submit\" value=\"Submit\" />"
								+ "</form>" + "</body>" + "</html>");
		response.flushBuffer();
	}
}