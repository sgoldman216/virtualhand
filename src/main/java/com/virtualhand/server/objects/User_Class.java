package main.java.com.virtualhand.server.objects;

import java.io.Serializable;
import java.sql.Timestamp;

public class User_Class implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -99304880137804412L;
	private long user_id;
	private long class_id;
	private int hand;
	private Timestamp date;
	
	public User_Class(long user_id, long class_id, int hand){
		this.user_id = user_id;
		this.class_id = class_id;
		this.hand = hand;
	}

	/**
	 * @return the date
	 */
	public Timestamp getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Timestamp date) {
		this.date = date;
	}

	/**
	 * @return the hand
	 */
	public int getHand() {
		return hand;
	}

	/**
	 * @return the user_id
	 */
	public long getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return the class_id
	 */
	public long getClass_id() {
		return class_id;
	}

	/**
	 * @param class_id the class_id to set
	 */
	public void setClass_id(long class_id) {
		this.class_id = class_id;
	}

	/**
	 * @return the hand
	 */
	public int isHand() {
		return hand;
	}

	/**
	 * @param hand the hand to set
	 */
	public void setHand(int hand) {
		this.hand = hand;
	}
}
