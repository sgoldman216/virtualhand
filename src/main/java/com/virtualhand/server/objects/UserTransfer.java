/***********************************************************
 * 
 * Copyright (c) 2007-2014 www.cropster.com
 * 
 * Created: Jul 15, 2014
 * 
 * Author: martin/cropster
 * 
 ***********************************************************/
package main.java.com.virtualhand.server.objects;

import java.util.Map;

/**
 * User token data, including the users name and his roles. The token can be
 * used to access resources that require authentication.
 */
public class UserTransfer
{

    private final String name;

    private final Map<String, Boolean> roles;

    private final String token;

    public UserTransfer(String userName, Map<String, Boolean> roles,
            String token)
    {

        this.name = userName;
        this.roles = roles;
        this.token = token;
    }

    public String getName()
    {

        return this.name;
    }

    public Map<String, Boolean> getRoles()
    {

        return this.roles;
    }

    public String getToken()
    {

        return this.token;
    }

}
