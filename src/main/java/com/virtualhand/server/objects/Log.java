package main.java.com.virtualhand.server.objects;

import java.sql.Timestamp;

public class Log {
	
	private long id;
	private Timestamp timestamp;
	private String type;
	private String location;
	private String message;
	
	/**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Log [id=" + id + ", timestamp=" + timestamp.toString() + ", type=" + type
				+ ", location=" + location + ", message=" + message + "]";
	}

	/**
	 * @param id
	 * @param timestamp
	 * @param type
	 * @param location
	 * @param message
	 */
	public Log(long id, Timestamp timestamp, String type, String location,
			String message) {
		this.id = id;
		this.timestamp = timestamp;
		this.type = type;
		this.location = location;
		this.message = message;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

}
