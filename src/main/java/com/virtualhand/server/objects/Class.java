package main.java.com.virtualhand.server.objects;

import java.io.Serializable;

public class Class implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5103159832707036994L;
	private long id;
	private String className;
	private String school;
	
	/**
	 * @param id
	 * @param className
	 * @param school
	 */
	public Class(long id, String className, String school) {
		this.id = id;
		this.className = className;
		this.school = school;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the school
	 */
	public String getSchool() {
		return school;
	}

	/**
	 * @param school the school to set
	 */
	public void setSchool(String school) {
		this.school = school;
	}
	
	 
}
