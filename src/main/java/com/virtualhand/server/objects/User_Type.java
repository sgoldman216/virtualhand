package main.java.com.virtualhand.server.objects;

import java.io.Serializable;

public class User_Type implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6590287145776647355L;
	private long user_id;
	private String type;
	
	public User_Type(long user_id, String type){
		this.user_id = user_id;
		this.type = type;
	}

	/**
	 * @return the user_id
	 */
	public long getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
}
