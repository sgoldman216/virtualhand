package main.java.com.virtualhand.server.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import main.java.com.virtualhand.server.objects.User_Class;

public class AppUser implements Serializable, UserDetails {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3657775534835606344L;
	private long id;
	private String username;
	private String password;
	private String userType;
	private List<User_Class> User_Classes;

	public AppUser(long id, String username, String password) {
		this.id = id;
		this.username = username;
		this.password = password;
		User_Classes = new ArrayList<User_Class>();
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @param userType
	 *            the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * @return the User_Classes
	 */
	public List<User_Class> getUser_Classes() {
		return User_Classes;
	}

	/**
	 * @param User_Classes
	 *            the User_Classes to set
	 */
	public void setUser_Classes(List<User_Class> User_Classes) {
		this.User_Classes = User_Classes;
	}

	public void addUser_Class(User_Class addUser_Class) {
		this.User_Classes.add(addUser_Class);
	}

	public void removeUser_Class(User_Class removeUser_Class) {
		this.User_Classes.remove(removeUser_Class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String str = "[AppUser[id=" + id + ", username=" + username
				+ ", userType=" + userType + ", User_Classes=[";
		for(User_Class cls : this.User_Classes){
			str += "User_Class[user_id=" + cls.getUser_id() + ",class_id=" + cls.getClass_id() + "], ";
		}
		str = str.substring(0, str.length() - 2);
		str += "]]]";
		return str;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		String type = this.getUserType();

		if (type == null) {
			return Collections.emptyList();
		}

		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();

		authorities.add(new SimpleGrantedAuthority(type));

		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO add account expiration capability - LOW
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO add account locking capability - LOW
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO add credetial expiration capability - VERY LOW
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO add enabling/disabling capability - MED
		return true;
	}
}
