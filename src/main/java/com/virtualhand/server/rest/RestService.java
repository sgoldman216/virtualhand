package main.java.com.virtualhand.server.rest;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.glassfish.jersey.server.ResourceConfig;

public class RestService extends ResourceConfig{
	@SuppressWarnings("deprecation")
	public RestService(){
		packages("main.java.com.virtualhand.server.rest.resources");
		Logger.getLogger(RestService.class).log(Priority.INFO, "REST SERVICE");
	}
}
