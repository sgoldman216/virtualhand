package main.java.com.virtualhand.server.rest.resources;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import main.java.com.virtualhand.server.authentication.AuthenticationManager;
import main.java.com.virtualhand.server.database.AppUserManager;
import main.java.com.virtualhand.server.objects.AppUser;
import main.java.com.virtualhand.server.objects.User_Class;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.transaction.annotation.Transactional;

@Path("/test")
@Produces(MediaType.TEXT_PLAIN)
@Transactional
public class TestResource {

	@Autowired
	private UserDetailsService userService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	@Autowired
	@Qualifier("springAuthenticationManager")
	private org.springframework.security.authentication.AuthenticationManager springAuthenticationManager;

	@Autowired
	@Qualifier("unauthorizedEntryPoint")
	private AuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
	@Qualifier("passwordEncoder")
	private PasswordEncoder passwordEncoder;

	@Autowired
	AppUserManager userManager;

	@Path("hello")
	@GET
	public String testHello() throws SQLException, NamingException {		
		return userManager.getAll().toString();
	}
	
	@Path("register")
	@POST
	public String studentRegister(@FormParam("username") String username,
			@FormParam("password") String password,
			@FormParam("firstclass") String firstClass,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws IOException,
			ServletException, SQLException, NumberFormatException, NamingException {
		if (password == null || password.equals("")) {
			return "PASSWORD REQUIRED!";
		}
		if (username == null || username.equals("")) {
			return "USERNAME REQUIRED!";
		}
		if (firstClass == null || firstClass.equals("")) {
			return "FIRSTCLASS REQUIRTED!";
		}
		try {
			Long.parseLong(firstClass);
		} catch (NumberFormatException e) {
			return "FIRSTCLASS MUST BE A NUMBER";
		}
		AppUser user = null;
		password = passwordEncoder.encode(password);
		try {
			user = (AppUser) userService.loadUserByUsername(username);
		} catch (UsernameNotFoundException e) {
		}
		if (user != null) {
			return "Username is already taken, please choose a new one.";
		} else {
			user = new AppUser(0, username, password);
			user.setUserType("student");
			this.userManager.save(user);
			user.addUser_Class(new User_Class(this.userManager.getByUsername(
					username).getId(), Long.parseLong(firstClass), 0));
			return "REGISTERED SUCCESFULLY";
		}
	}
}
