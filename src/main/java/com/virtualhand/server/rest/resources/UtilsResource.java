package main.java.com.virtualhand.server.rest.resources;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import main.java.com.virtualhand.server.authentication.AuthenticationManager;
import main.java.com.virtualhand.server.database.AppUserManager;
import main.java.com.virtualhand.server.database.ClassManager;
import main.java.com.virtualhand.server.database.KeyManager;
import main.java.com.virtualhand.server.database.LogManager;
import main.java.com.virtualhand.server.objects.Key;
import main.java.com.virtualhand.server.objects.Log;
import main.java.com.virtualhand.server.objects.UserTransfer;
import main.java.com.virtualhand.server.responsemessages.ResponseMessages;
import main.java.com.virtualhand.server.security.KeyGenerator;
import main.java.com.virtualhand.server.security.TokenUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.transaction.annotation.Transactional;

@Path("/utils")
@Produces(MediaType.TEXT_PLAIN)
@Transactional
public class UtilsResource {

	@Autowired
	private UserDetailsService userService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	@Autowired
	@Qualifier("springAuthenticationManager")
	private org.springframework.security.authentication.AuthenticationManager springAuthenticationManager;

	@Autowired
	@Qualifier("unauthorizedEntryPoint")
	private AuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
	@Qualifier("passwordEncoder")
	private PasswordEncoder passwordEncoder;

	@Autowired
	AppUserManager userManager;

	@Autowired
	ClassManager classManager;
	
	@Autowired
	KeyGenerator keyGenerator;
	
	@Autowired
	KeyManager keyManager;
	
	@Autowired
	LogManager logManager;

	@SuppressWarnings("deprecation")
	@Path("authenticate")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public UserTransfer authenticate(@FormParam("username") String username,
			@FormParam("password") String password,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws IOException,
			ServletException, SQLException, NamingException {
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
				username, password);
		try {
			Authentication authentication = springAuthenticationManager
					.authenticate(authenticationToken);
			SecurityContextHolder.getContext()
					.setAuthentication(authentication);
		} catch (AuthenticationException failed) {
			SecurityContextHolder.clearContext();
			Logger.global.log(java.util.logging.Level.INFO,
					"Authentication Failed! Oopsies!");
			authenticationEntryPoint.commence(request, response, failed);
			logManager.save(new Log(-1, null, "info", "server", "UtilsResource.authenticate(Username:" + username + ") was called. Result: Failure"));
			return null;
		}

		Map<String, Boolean> roles = new HashMap<String, Boolean>();

		UserDetails userDetails = userService.loadUserByUsername(username);

		for (GrantedAuthority authority : userDetails.getAuthorities()) {
			roles.put(authority.toString(), Boolean.TRUE);
		}

		logManager.save(new Log(-1, null, "info", "server", "UtilsResource.authenticate(Username:" + username + ") was called. Result: Success"));
		return new UserTransfer(userDetails.getUsername(), roles,
				TokenUtils.createToken(userDetails));
	}

	@Path("checkForClass")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String checkForClass(@QueryParam("class_id") String class_id,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException, NamingException {
		long CLASS_ID = 0;
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			return ResponseMessages.INVALID_LONG;
		}
		if (classManager.get(CLASS_ID).isEmpty()) {
			return "false";
		} else {
			return "true";
		}
	}
	
	@Path("getPurchaseKey")
	@POST
	public String getPurchaseKey(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException, NamingException{
		String key = this.keyGenerator.getRandomKey();
		keyManager.save(new Key(0, key, 1));
		logManager.save(new Log(-1, null, "info", "server", "UtilsResource.getPurchaseKey() was called. Result: Success (" + key + ")"));
		return key;
	}
}
