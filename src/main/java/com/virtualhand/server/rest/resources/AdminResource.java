package main.java.com.virtualhand.server.rest.resources;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import main.java.com.virtualhand.server.authentication.AuthenticationManager;
import main.java.com.virtualhand.server.database.AppUserManager;
import main.java.com.virtualhand.server.database.ClassManager;
import main.java.com.virtualhand.server.database.LogManager;
import main.java.com.virtualhand.server.objects.AppUser;
import main.java.com.virtualhand.server.objects.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.transaction.annotation.Transactional;

@Path("/admin")
@Produces(MediaType.TEXT_PLAIN)
@Transactional
public class AdminResource {

	private static final int PAGE_SIZE = 30;
	
	@Autowired
	private UserDetailsService userService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	@Autowired
	@Qualifier("springAuthenticationManager")
	private org.springframework.security.authentication.AuthenticationManager springAuthenticationManager;

	@Autowired
	@Qualifier("unauthorizedEntryPoint")
	private AuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
	@Qualifier("passwordEncoder")
	private PasswordEncoder passwordEncoder;

	@Autowired
	AppUserManager userManager;

	@Autowired
	ClassManager classManager;

	@Autowired
	LogManager logManager;

	// FIXME Build AdminResource

	@Path("register")
	@POST
	public String adminRegister(@FormParam("username") String username,
			@FormParam("password") String password,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws IOException,
			ServletException, SQLException, NamingException {
		if (password == null || password.equals("")) {
			return "PASSWORD REQUIRED!";
		}
		if (username == null || username.equals("")) {
			return "USERNAME REQUIRED!";
		}
		AppUser user = null;
		password = passwordEncoder.encode(password);
		try {
			user = (AppUser) userService.loadUserByUsername(username);
		} catch (UsernameNotFoundException e) {
		}
		if (user != null) {
			return "Username is already taken, please choose a new one.";
		} else {
			user = new AppUser(0, username, password);
			user.setUserType("admin");
			this.userManager.save(user);
			return "REGISTERED SUCCESFULLY";
		}
	}

	@Path("getAllLogs")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllLogs(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		List<Log> logs = logManager.getAll();
		String ret = "";
		for(Log log : logs) {
			ret += "[" + log.toString() + "];";
		}
		ret = ret.substring(0, ret.length() - 1);
		return ret;

	}
	
	@Path("getAllUsers")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllUsers(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException, NamingException{
		List<AppUser> users = userManager.getAll();
		String ret = "";
		for (AppUser user : users) {
			ret += "[" + user.toString() + "];";
		}
		ret = ret.substring(0, ret.length() - 1);
		return ret;
	}

	@Path("getErrorLogs")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String getErrorLogs(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		List<Log> logs = logManager.getAll();
		String ret = "";
		for (Log log : logs) {
			if (log.getType().equals("error")) {
				ret += "[" + log.toString() + "];";
			}
		}
		ret = ret.substring(0, ret.length() - 1);
		return ret;
	}
	
	@Path("getFatalLogs")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String getFatalLogs(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		List<Log> logs = logManager.getAll();
		String ret = "";
		for (Log log : logs) {
			if (log.getType().equals("fatal")) {
				ret += "[" + log.toString() + "];";
			}
		}
		ret = ret.substring(0, ret.length() - 1);
		return ret;
	}
	
	@Path("getInfoLogs")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String getInfoLogs(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		List<Log> logs = logManager.getAll();
		String ret = "";
		for (Log log : logs) {
			if (log.getType().equals("info")) {
				ret += "[" + log.toString() + "];";
			}
		}
		ret = ret.substring(0, ret.length() - 1);
		return ret;
	}
	
	@Path("getWarningLogs")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String getWarningLogs(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		List<Log> logs = logManager.getAll();
		String ret = "";
		for (Log log : logs) {
			if (log.getType().equals("warning")) {
				ret += "[" + log.toString() + "];";
			}
		}
		ret = ret.substring(0, ret.length() - 1);
		return ret;
	}
	
	@Path("getWithinDateParameters")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String getLogsWithinDates(@FormParam("begin") String begin, @FormParam("end") String end,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		List<Log> logs = logManager.getAll();
		String ret = "";
		Timestamp B = Timestamp.valueOf(begin);
		Timestamp E = Timestamp.valueOf(end);
		for (Log log : logs) {
			if(log.getTimestamp().after(B) && log.getTimestamp().before(E)){
				ret += "[" + log.toString() + "];";
			}
		}
		ret = ret.substring(0, ret.length() - 1);
		return ret;
	}
}
