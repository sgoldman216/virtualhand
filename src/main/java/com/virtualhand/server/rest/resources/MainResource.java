package main.java.com.virtualhand.server.rest.resources;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import main.java.com.virtualhand.server.authentication.AuthenticationManager;
import main.java.com.virtualhand.server.database.AppUserManager;
import main.java.com.virtualhand.server.database.ClassManager;
import main.java.com.virtualhand.server.objects.UserTransfer;
import main.java.com.virtualhand.server.security.TokenUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.transaction.annotation.Transactional;


@Path("/main")
@Produces(MediaType.TEXT_PLAIN)
@Transactional
public class MainResource {

	@Autowired
	private UserDetailsService userService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	@Autowired
	@Qualifier("springAuthenticationManager")
	private org.springframework.security.authentication.AuthenticationManager springAuthenticationManager;

	@Autowired
	@Qualifier("unauthorizedEntryPoint")
	private AuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
	@Qualifier("passwordEncoder")
	private PasswordEncoder passwordEncoder;

	@Autowired
	AppUserManager userManager;

	@Autowired
	ClassManager classManager;

	@Path("authenticate")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public UserTransfer authenticate(@FormParam("username") String username,
			@FormParam("password") String password,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws IOException,
			ServletException {
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
				username, password);
		try {
			Authentication authentication = springAuthenticationManager
					.authenticate(authenticationToken);
			SecurityContextHolder.getContext()
					.setAuthentication(authentication);
		} catch (AuthenticationException failed) {
			SecurityContextHolder.clearContext();

			authenticationEntryPoint.commence(request, response, failed);
			return null;
		}

		Map<String, Boolean> roles = new HashMap<String, Boolean>();

		/*
		 * Reload user as password of authentication principal will be null
		 * after authorization and password is needed for token generation
		 */
		UserDetails userDetails = userService.loadUserByUsername(username);

		for (GrantedAuthority authority : userDetails.getAuthorities()) {
			roles.put(authority.toString(), Boolean.TRUE);
		}

		return new UserTransfer(userDetails.getUsername(), roles,
				TokenUtils.createToken(userDetails));
	}	
}
