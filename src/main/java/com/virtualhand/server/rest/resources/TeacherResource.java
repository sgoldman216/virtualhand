package main.java.com.virtualhand.server.rest.resources;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import main.java.com.virtualhand.server.authentication.AuthenticationManager;
import main.java.com.virtualhand.server.database.AppUserManager;
import main.java.com.virtualhand.server.database.ClassManager;
import main.java.com.virtualhand.server.database.KeyManager;
import main.java.com.virtualhand.server.database.LogManager;
import main.java.com.virtualhand.server.objects.AppUser;
import main.java.com.virtualhand.server.objects.Class;
import main.java.com.virtualhand.server.objects.Key;
import main.java.com.virtualhand.server.objects.Log;
import main.java.com.virtualhand.server.objects.User_Class;
import main.java.com.virtualhand.server.responsemessages.ResponseMessages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.transaction.annotation.Transactional;

@Path("/teacher")
@Produces(MediaType.TEXT_PLAIN)
@Transactional
public class TeacherResource {

	@Autowired
	private UserDetailsService userService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	@Autowired
	@Qualifier("springAuthenticationManager")
	private org.springframework.security.authentication.AuthenticationManager springAuthenticationManager;

	@Autowired
	@Qualifier("unauthorizedEntryPoint")
	private AuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
	@Qualifier("passwordEncoder")
	private PasswordEncoder passwordEncoder;

	@Autowired
	AppUserManager userManager;

	@Autowired
	ClassManager classManager;

	@Autowired
	KeyManager keyManager;
	
	@Autowired
	LogManager logManager;

	@Path("register")
	@POST
	public String teacherRegister(@FormParam("username") String username,
			@FormParam("password") String password,
			@FormParam("classname") String classname,
			@FormParam("school") String school,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws IOException,
			ServletException, SQLException, NamingException {
		if (password == null || password.equals("")) {
			logManager.save(new Log(-1, null, "info", "server", "TeacherResource.teacherRegister(Username:" + username + ",classname" + "," + classname + ",school" + school + ") was called. Result: " + ResponseMessages.PASSWORD_NOT_SENT));
			return ResponseMessages.PASSWORD_NOT_SENT;
		}
		if (username == null || username.equals("")) {
			logManager.save(new Log(-1, null, "info", "server", "TeacherResource.teacherRegister(Username:" + username + ",classname" + "," + classname + ",school" + school + ") was called. Result: " + ResponseMessages.USERNAME_NOT_SENT));
			return ResponseMessages.USERNAME_NOT_SENT;
		}
		if (classname == null || classname.equals("")) {
			logManager.save(new Log(-1, null, "info", "server", "TeacherResource.teacherRegister(Username:" + username + ",classname" + "," + classname + ",school" + school + ") was called. Result: Classname not sent"));
			return ResponseMessages.VARIABLE_NOT_SENT;
		}
		if (school == null || school.equals("")) {
			logManager.save(new Log(-1, null, "info", "server", "TeacherResource.teacherRegister(Username:" + username + ",classname" + "," + classname + ",school" + school + ") was called. Result: school not sent"));
			return ResponseMessages.VARIABLE_NOT_SENT;
		}
		AppUser user = null;
		Class c = null;
		password = passwordEncoder.encode(password);
		try {
			user = (AppUser) userService.loadUserByUsername(username);
		} catch (UsernameNotFoundException e) {
		}
		try {
			c = this.classManager.getByClassNameAndSchool(classname, school);
		} catch (Exception e) {
		}
		if (user != null) {
			logManager.save(new Log(-1, null, "info", "server", "TeacherResource.teacherRegister(Username:" + username + ",classname" + "," + classname + ",school" + school + ") was called. Result: Non-Unique Username"));
			return ResponseMessages.NON_UNIQUE_USERNAME;
		}
		if (c != null) {
			logManager.save(new Log(-1, null, "info", "server", "TeacherResource.teacherRegister(Username:" + username + ",classname" + "," + classname + ",school" + school + ") was called. Result: Non-Unique school-class pair"));
			return ResponseMessages.NON_UNIQUE_CLASS_SCHOOL_PAIR;
		} else
			try {

				user = new AppUser(0, username, password);
				user.setUserType("teacher-basic");
				this.userManager.save(user);
				user = this.userManager.getByUsername(username);
				Class cls = new Class(0, classname, school);
				this.classManager.save(cls);
				cls = this.classManager.getByClassNameAndSchool(classname,
						school);
				user.addUser_Class(new User_Class(this.userManager
						.getByUsername(username).getId(), cls.getId(), 0));
				this.userManager.update(user);
				logManager.save(new Log(-1, null, "info", "server", "UserID: " + user.getId() + " : TeacherResource.teacherRegister(Username:" + username + ",classname" + "," + classname + ",school" + school + ") was called. Result: Success"));
				return ResponseMessages.SUCCESS;

			} catch (Exception e) {
				logManager.save(new Log(-1, null, "error", "server", "TeacherResource.teacherRegister(Username:" + username + ",classname" + "," + classname + ",school" + school + ") was called. Result: " + e.getMessage()));
			}
		logManager.save(new Log(-1, null, "error", "server", "TeacherResource.teacherRegister(Username:" + username + ",classname" + "," + classname + ",school" + school + ") was called. Result: Unknown Failure"));
		return ResponseMessages.OTHER_FAILURE;
	}

	@Path("setHandStatus")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String setHandStatus(@FormParam("id") String id,
			@FormParam("class_id") String class_id,
			@FormParam("handstate") String handstate,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		AppUser teacher = authenticationManager.getAuthenticatedUser();
		long ID = 0;
		long CLASS_ID = 0;
		int STATE = 0;
		try {
			ID = Long.parseLong(id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.setHandStatus(id:" + id + ",class_id" + "," + class_id + ",handstate" + handstate + ") was called. Result: id not a long"));
			return ResponseMessages.INVALID_LONG;
		}
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.setHandStatus(id:" + id + ",class_id" + "," + class_id + ",handstate" + handstate + ") was called. Result: class_id not a long"));
			return ResponseMessages.INVALID_LONG;
		}
		try {
			STATE = Integer.parseInt(handstate);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.setHandStatus(id:" + id + ",class_id" + "," + class_id + ",handstate" + handstate + ") was called. Result: handstate not an integer"));
			return ResponseMessages.INVALID_INTEGER;
		}

		if (!(STATE == 1 || STATE == 2)) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.setHandStatus(id:" + id + ",class_id" + "," + class_id + ",handstate" + handstate + ") was called. Result: handstate not a valid state (1 or 2)"));
			return ResponseMessages.INVALID_HANDSTATE;
		}

		

		boolean hasClass = false;
		for (User_Class c : teacher.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				hasClass = true;
				break;
			}
		}

		if (!hasClass) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.setHandStatus(id:" + id + ",class_id" + "," + class_id + ",handstate" + handstate + ") was called. Result: teacher is not authorized for this class"));
			return ResponseMessages.UNAUTHORIZED_TEACHER;
		}

		AppUser user = userManager.get(ID).get(0);
		if (!user.getUserType().equals("student")) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.setHandStatus(id:" + id + ",class_id" + "," + class_id + ",handstate" + handstate + ") was called. Result: id does not belong to student"));
			return ResponseMessages.INVALID_ID;
		}

		boolean inClass = false;
		for (User_Class c : user.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				inClass = true;
				c.setHand(STATE);
				break;
			}
		}

		if (!inClass) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.setHandStatus(id:" + id + ",class_id" + "," + class_id + ",handstate" + handstate + ") was called. Result: student is not in specified class"));
			return ResponseMessages.INVALID_STUDENT;
		}

		userManager.update(user);

		logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.setHandStatus(id:" + id + ",class_id" + "," + class_id + ",handstate" + handstate + ") was called. Result: Success"));
		return ResponseMessages.SUCCESS;
	}

	@Path("addNewClass")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String addClass(@FormParam("classname") String classname,
			@FormParam("school") String school,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		List<Class> classes = classManager.getAll();
		AppUser teacher = authenticationManager.getAuthenticatedUser();
		if (teacher.getUserType().equals("teacher-basic")
				&& teacher.getUser_Classes().size() >= 2) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.addNewClass(classname:" + classname + ",school" + "," + school + ") was called. Result: Class limit reached"));
			return ResponseMessages.TOO_MANY_CLASSES;
		}
		for (Class c : classes) {
			if (c.getClassName().equals(classname)
					&& c.getSchool().equals(school)) {
				logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.addNewClass(classname:" + classname + ",school" + "," + school + ") was called. Result: Non-Unique Classname-School Pair"));
				return ResponseMessages.NON_UNIQUE_CLASS_SCHOOL_PAIR;
			}
		}
		Class cls = new Class(0, classname, school);
		classManager.save(cls);
		cls = classManager.getByClassNameAndSchool(classname, school);

		teacher.addUser_Class(new User_Class(teacher.getId(), cls.getId(), 0));
		userManager.update(teacher);
		logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.addNewClass(classname:" + classname + ",school" + "," + school + ") was called. Result: Success"));
		return ResponseMessages.SUCCESS + "," + cls.getId();
	}

	@Path("changePassword")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String changePassword(@FormParam("password") String password,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		AppUser teacher = authenticationManager.getAuthenticatedUser();
		teacher.setPassword(passwordEncoder.encode(password));
		userManager.update(teacher);
		logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.changePassword(password:" + password + ") was called. Result: Success"));
		return ResponseMessages.SUCCESS;
	}

	@Path("getHandsForClass")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String getHandsForClass(@FormParam("class_id") String class_id,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		long CLASS_ID = 0l;
		AppUser teacher = authenticationManager.getAuthenticatedUser();
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.getHandsForClass(class_id:" + class_id + ") was called. Result: Class_id not a valid long"));
			return ResponseMessages.INVALID_LONG;
		}

		

		boolean hasClass = false;
		for (User_Class c : teacher.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				hasClass = true;
				break;
			}
		}

		if (!hasClass) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.getHandsForClass(class_id:" + class_id + ") was called. Result: Teacher not authorized for class"));
			return ResponseMessages.UNAUTHORIZED_TEACHER;
		}

		List<AppUser> users = userManager.getAll();
		String ret = "";
		outer: for (AppUser user : users) {
			if(user.getId() == teacher.getId()){
				continue;
			}
			for (User_Class c : user.getUser_Classes()) {
				if (c.getClass_id() == CLASS_ID) {
					ret += "[" + user.getUsername() + ",";
					ret += c.isHand() + ",";
					try {
						ret += c.getDate().toString() + "];";
					} catch (NullPointerException e) {
						ret = ret.substring(0, ret.length() - 1);
						ret += "];";
					}
					continue outer;
				}
			}
		}
		ret = ret.substring(0, ret.length() - 1);
		//logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.getHandsForClass(class_id:" + class_id + ") was called. Result: Success"));
		return ret;
	}

	@Path("getStudents")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String getStudents(@FormParam("class_id") String class_id,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		long CLASS_ID = 0l;
		AppUser teacher = authenticationManager.getAuthenticatedUser();
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.getStudents(class_id:" + class_id + ") was called. Result: Class_id is an invalid long"));
			return ResponseMessages.INVALID_LONG;
		}

		

		boolean hasClass = false;
		for (User_Class c : teacher.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				hasClass = true;
				break;
			}
		}

		if (!hasClass) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.getStudents(class_id:" + class_id + ") was called. Result: Teacher is not authorized for class"));
			return ResponseMessages.UNAUTHORIZED_TEACHER;
		}

		List<AppUser> users = userManager.getAll();
		String ret = "";
		outer: for (AppUser user : users) {
			if(user.getId() == teacher.getId()){
				continue;
			}
			for (User_Class c : user.getUser_Classes()) {
				if (c.getClass_id() == CLASS_ID) {
					ret += "[" + user.getUsername() + "," + user.getId() + "];";
					continue outer;
				}
			}
		}
		ret = ret.substring(0, ret.length() - 1);
		//logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.getStudents(class_id:" + class_id + ") was called. Result: Success"));
		return ret;
	}

	@Path("removeClass")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String removeClass(@FormParam("class_id") String class_id,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		long CLASS_ID = 0l;
		AppUser teacher = authenticationManager.getAuthenticatedUser();
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.removeClass(class_id:" + class_id + ") was called. Result: Class_id was an invalid long"));
			return ResponseMessages.INVALID_LONG;
		}

		

		boolean hasClass = false;
		for (User_Class c : teacher.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				hasClass = true;
				break;
			}
		}

		if (!hasClass) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.removeClass(class_id:" + class_id + ") was called. Result: Teacher is unauthorized for that class"));
			return ResponseMessages.UNAUTHORIZED_TEACHER;
		}

		List<AppUser> users = userManager.getAll();
		// users.add(teacher);
		outer: for (AppUser user : users) {
			for (int i = 0; i < user.getUser_Classes().size(); i++) {
				User_Class c = user.getUser_Classes().get(i);
				if (c.getClass_id() == CLASS_ID) {
					user.removeUser_Class(c);
					userManager.update(user);
					continue outer;
				}
			}
		}
		Class cls = classManager.get(CLASS_ID).get(0);
		classManager.remove(cls);
		logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.removeClass(class_id:" + class_id + ") was called. Result: Success"));
		return ResponseMessages.SUCCESS;
	}

	@Path("getClasses")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String getClasses(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		AppUser teacher = authenticationManager.getAuthenticatedUser();
		String ret = "";
		for (User_Class c : teacher.getUser_Classes()) {
			ret += "["
					+ classManager.get(c.getClass_id()).get(0).getClassName()
					+ "," + c.getClass_id() + "];";
		}
		ret = ret.substring(0, ret.length() - 1);
		logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.getClasses() was called. Result: Success"));
		return ret;
	}

	@Path("removeStudent")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String removeStudent(@FormParam("id") String id,
			@FormParam("class_id") String class_id,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		long ID = 0;
		long CLASS_ID = 0;
		AppUser teacher = authenticationManager.getAuthenticatedUser();
		try {
			ID = Long.parseLong(id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.removeStudent(id:" + id + ",class_id:" + class_id + ") was called. Result: id is an invalid long"));
			return ResponseMessages.INVALID_LONG;
		}
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.removeStudent(id:" + id + ",class_id:" + class_id + ") was called. Result: class_id is an invalid long"));
			return ResponseMessages.INVALID_LONG;
		}

		boolean hasClass = false;
		for (User_Class c : teacher.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				hasClass = true;
				break;
			}
		}

		if (!hasClass) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.removeStudent(id:" + id + ",class_id:" + class_id + ") was called. Result: teacher is unauthorized for class"));
			return ResponseMessages.UNAUTHORIZED_TEACHER;
		}

		AppUser user = userManager.get(ID).get(0);
		for (int i = 0; i < user.getUser_Classes().size(); i++) {
			User_Class cls = user.getUser_Classes().get(i);
			if (cls.getClass_id() == CLASS_ID) {
				user.removeUser_Class(cls);
				i--;
			}
		}
		userManager.update(user);
		logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.removeStudent(id:" + id + ",class_id:" + class_id + ") was called. Result: Success"));
		return ResponseMessages.SUCCESS;
	}

	@Path("addKey")
	@POST
	public String addKey(@FormParam("key") String key,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		AppUser teacher = authenticationManager.getAuthenticatedUser();
		List<Key> keys = keyManager.getAll();
		for (Key k : keys) {
			if (k.getKey().equals(key)) {
				keyManager.remove(k);
				teacher.setUserType("teacher-premium");
				userManager.update(teacher);
				logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.addKey(key:" + key + ") was called. Result: Success"));
				return ResponseMessages.SUCCESS;
			}
		}
		logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.addKey(key:" + key + ") was called. Result: Invalid Key"));
		return ResponseMessages.INVALID_KEY;
	}

	@Path("acceptStudent")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String acceptStudent(@FormParam("id") String id,
			@FormParam("class_id") String class_id,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		AppUser teacher = authenticationManager.getAuthenticatedUser();

		long ID = 0;
		long CLASS_ID = 0;
		try {
			ID = Long.parseLong(id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.acceptStudent(id:" + id + ",class_id" + class_id + ") was called. Result: id is an invalid long"));
			return ResponseMessages.INVALID_LONG;
		}
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.acceptStudent(id:" + id + ",class_id" + class_id + ") was called. Result: class_id is an invalid long"));
			return ResponseMessages.INVALID_LONG;
		}
		
		boolean hasClass = false;
		for (User_Class c : teacher.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				hasClass = true;
				break;
			}
		}

		if (!hasClass) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.acceptStudent(id:" + id + ",class_id" + class_id + ") was called. Result: teacher is not authorized for class"));
			return ResponseMessages.UNAUTHORIZED_TEACHER;
		}

		AppUser user = userManager.get(ID).get(0);
		if (!user.getUserType().equals("student")) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.acceptStudent(id:" + id + ",class_id" + class_id + ") was called. Result: id does not represent a student"));
			return ResponseMessages.INVALID_ID;
		}

		boolean inClass = false;
		for (User_Class c : user.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				inClass = true;
				c.setHand(1);
				break;
			}
		}

		if (!inClass) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.acceptStudent(id:" + id + ",class_id" + class_id + ") was called. Result: student is not in the specified class"));
			return ResponseMessages.INVALID_STUDENT;
		}
		
		userManager.update(user);
		logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.acceptStudent(id:" + id + ",class_id" + class_id + ") was called. Result: Success"));
		return ResponseMessages.SUCCESS;
	}
	
	@Path("refuseStudent")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String refuseStudent(@FormParam("id") String id,
			@FormParam("class_id") String class_id,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		AppUser teacher = authenticationManager.getAuthenticatedUser();

		long ID = 0;
		long CLASS_ID = 0;
		try {
			ID = Long.parseLong(id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.refuseStudent(id:" + id + ",class_id" + class_id + ") was called. Result: id is not a valid long"));
			return ResponseMessages.INVALID_LONG;
		}
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.refuseStudent(id:" + id + ",class_id" + class_id + ") was called. Result: class_id is not a valid long"));
			return ResponseMessages.INVALID_LONG;
		}
		
		boolean hasClass = false;
		for (User_Class c : teacher.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				hasClass = true;
				break;
			}
		}

		if (!hasClass) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.refuseStudent(id:" + id + ",class_id" + class_id + ") was called. Result: teacher is not authorized for class"));
			return ResponseMessages.UNAUTHORIZED_TEACHER;
		}

		AppUser user = userManager.get(ID).get(0);
		if (!user.getUserType().equals("student")) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.refuseStudent(id:" + id + ",class_id" + class_id + ") was called. Result: id does not represent a student"));
			return ResponseMessages.INVALID_ID;
		}

		boolean inClass = false;
		for (User_Class c : user.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				inClass = true;
				user.removeUser_Class(c);
				break;
			}
		}

		if (!inClass) {
			logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.refuseStudent(id:" + id + ",class_id" + class_id + ") was called. Result: student is not in class"));
			return ResponseMessages.INVALID_STUDENT;
		}
		
		userManager.update(user);
		logManager.save(new Log(-1, null, "info", "server", "UserID: " + teacher.getId() + " : TeacherResource.refuseStudent(id:" + id + ",class_id" + class_id + ") was called. Result: Success"));
		return ResponseMessages.SUCCESS;
	}
}
