package main.java.com.virtualhand.server.rest.resources;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import main.java.com.virtualhand.server.authentication.AuthenticationManager;
import main.java.com.virtualhand.server.database.AppUserManager;
import main.java.com.virtualhand.server.database.ClassManager;
import main.java.com.virtualhand.server.database.LogManager;
import main.java.com.virtualhand.server.objects.AppUser;
import main.java.com.virtualhand.server.objects.Class;
import main.java.com.virtualhand.server.objects.Log;
import main.java.com.virtualhand.server.objects.User_Class;
import main.java.com.virtualhand.server.responsemessages.ResponseMessages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.transaction.annotation.Transactional;

@Path("/student")
@Produces(MediaType.TEXT_PLAIN)
@Transactional
public class StudentResource {

	@Autowired
	private UserDetailsService userService;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;

	@Autowired
	@Qualifier("springAuthenticationManager")
	private org.springframework.security.authentication.AuthenticationManager springAuthenticationManager;

	@Autowired
	@Qualifier("unauthorizedEntryPoint")
	private AuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
	@Qualifier("passwordEncoder")
	private PasswordEncoder passwordEncoder;

	@Autowired
	AppUserManager userManager;

	@Autowired
	ClassManager classManager;

	@Autowired
	LogManager logManager;

	@Path("register")
	@POST
	public String studentRegister(@FormParam("username") String username,
			@FormParam("password") String password,
			@FormParam("firstclass") String firstClass,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws IOException,
			ServletException, SQLException, NamingException {
		if (password == null || password.equals("")) {
			logManager.save(new Log(-1, null, "info", "server",
					"StudentResource.studentRegister(Username:" + username
							+ ",firstclass" + "," + firstClass
							+ ") was called. Result: "
							+ ResponseMessages.PASSWORD_NOT_SENT));
			return ResponseMessages.PASSWORD_NOT_SENT;
		}
		if (username == null || username.equals("")) {
			logManager.save(new Log(-1, null, "info", "server",
					"StudentResource.studentRegister(Username:" + username
							+ ",firstclass" + "," + firstClass
							+ ") was called. Result: "
							+ ResponseMessages.USERNAME_NOT_SENT));
			return ResponseMessages.USERNAME_NOT_SENT;
		}
		if (firstClass == null || firstClass.equals("")) {
			logManager.save(new Log(-1, null, "info", "server",
					"StudentResource.studentRegister(Username:" + username
							+ ",firstclass" + "," + firstClass
							+ ") was called. Result: firstclass not sent"));
			return ResponseMessages.VARIABLE_NOT_SENT;
		}
		try {
			Long.parseLong(firstClass);
		} catch (NumberFormatException e) {
			logManager
					.save(new Log(
							-1,
							null,
							"info",
							"server",
							"StudentResource.studentRegister(Username:"
									+ username
									+ ",firstclass"
									+ ","
									+ firstClass
									+ ") was called. Result: first class is not a valid long"));
			return ResponseMessages.INVALID_LONG;
		}
		boolean exists = false;
		for (Class cls : classManager.getAll()) {
			if (cls.getId() == Long.parseLong(firstClass)) {
				exists = true;
				break;
			}
		}
		if (!exists) {
			return ResponseMessages.INVALID_CLASS_ID;
		}

		AppUser user = null;
		password = passwordEncoder.encode(password);
		try {
			user = (AppUser) userService.loadUserByUsername(username);
		} catch (Exception e) {
		}
		if (user != null) {
			logManager.save(new Log(-1, null, "info", "server",
					"StudentResource.studentRegister(Username:" + username
							+ ",firstclass" + "," + firstClass
							+ ") was called. Result: Non-Unique Username"));
			return ResponseMessages.NON_UNIQUE_USERNAME;
		} else {
			user = new AppUser(0, username, password);
			user.setUserType("student");
			this.userManager.save(user);
			user = this.userManager.getByUsername(username);
			user.addUser_Class(new User_Class(this.userManager.getByUsername(
					username).getId(), Long.parseLong(firstClass), 5));
			this.userManager.update(user);
			logManager.save(new Log(-1, null, "info", "server", "UserID: "
					+ user.getId()
					+ " : StudentResource.studentRegister(Username:" + username
					+ ",firstclass" + "," + firstClass
					+ ") was called. Result: Success"));
			return ResponseMessages.SUCCESS;
		}
	}

	@Path("getHandStatus")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String getHandStatus(@FormParam("class_id") String class_id,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			IOException, NamingException {
		long CLASS_ID = 0;
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			return ResponseMessages.INVALID_LONG;
		}
		AppUser user = authenticationManager.getAuthenticatedUser();
		List<User_Class> classes = user.getUser_Classes();
		for (User_Class uc : classes) {
			if (uc.getClass_id() == CLASS_ID) {
				return "" + uc.isHand();
			}
		}
		logManager.save(new Log(-1, null, "error", "server", "UserID: "
				+ user.getId() + " : StudentResource.getHandStatus(class_id:"
				+ class_id + ") was called. Result: Other failure"));
		return ResponseMessages.OTHER_FAILURE;
	}

	@Path("setHandStatus")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String setHandStatus(@FormParam("class_id") String class_id,
			@FormParam("handstate") String handstate,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		long CLASS_ID = 0;
		int STATE = 0;
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			return ResponseMessages.INVALID_LONG;
		}
		try {
			STATE = Integer.parseInt(handstate);
		} catch (NumberFormatException e) {
			return ResponseMessages.INVALID_LONG;
		}
		if (!(STATE == 1 || STATE == 2)) {
			return "INVALID STATE. STATE MUST BE 1 or 2";
		}
		AppUser user = authenticationManager.getAuthenticatedUser();
		List<User_Class> classes = user.getUser_Classes();
		for (User_Class uc : classes) {
			if (uc.getClass_id() == CLASS_ID) {
				uc.setHand(STATE);
				userManager.update(user);
				logManager.save(new Log(-1, null, "info", "server", "UserID: "
						+ user.getId()
						+ " : StudentResource.setHandStatus(class_id:"
						+ class_id + ",handstate:" + handstate
						+ ") was called. Result: Success"));
				return ResponseMessages.SUCCESS;
			}
		}
		logManager.save(new Log(-1, null, "error", "server", "UserID: "
				+ user.getId() + " : StudentResource.setHandStatus(class_id:"
				+ class_id + ",handstate:" + handstate
				+ ") was called. Result: Other failure"));
		return ResponseMessages.OTHER_FAILURE;
	}

	@Path("addClass")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String addClass(@FormParam("class_id") String class_id,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		AppUser user = authenticationManager.getAuthenticatedUser();
		long CLASS_ID = 0;
		try {
			CLASS_ID = Long.parseLong(class_id);
		} catch (NumberFormatException e) {
			return ResponseMessages.INVALID_LONG;
		}
		List<Class> classes = classManager.get(CLASS_ID);
		if (classes.isEmpty()) {
			return ResponseMessages.INVALID_CLASS_ID;
		}
		for (User_Class c : user.getUser_Classes()) {
			if (c.getClass_id() == CLASS_ID) {
				return ResponseMessages.SUCCESS;
			}
		}
		user.addUser_Class(new User_Class(user.getId(), CLASS_ID, 5));
		userManager.update(user);
		logManager.save(new Log(-1, null, "info", "server", "UserID: "
				+ user.getId() + " : StudentResource.addClass(class_id:"
				+ class_id + ") was called. Result: Success"));
		return ResponseMessages.SUCCESS;
	}

	@Path("changePassword")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String changePassword(@FormParam("password") String password,
			@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		AppUser user = authenticationManager.getAuthenticatedUser();
		user.setPassword(passwordEncoder.encode(password));
		userManager.update(user);
		logManager.save(new Log(-1, null, "info", "server", "UserID: "
				+ user.getId() + " : StudentResource.setHandStatus(password:"
				+ password + ") was called. Result: Success"));
		return ResponseMessages.SUCCESS;
	}

	@Path("getClasses")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String getClasses(@Context HttpServletRequest request,
			@Context HttpServletResponse response) throws SQLException,
			NamingException {
		AppUser user = authenticationManager.getAuthenticatedUser();
		String ret = "";
		for (User_Class c : user.getUser_Classes()) {
			ret += "["
					+ classManager.get(c.getClass_id()).get(0).getClassName()
					+ "," + c.getClass_id() + "];";
		}
		ret = ret.substring(0, ret.length() - 1);
		logManager
				.save(new Log(
						-1,
						null,
						"info",
						"server",
						"UserID: "
								+ user.getId()
								+ " : StudentResource.getClasses() was called. Result: Success"));
		return (ret);
	}
}
