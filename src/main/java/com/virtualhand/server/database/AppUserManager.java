package main.java.com.virtualhand.server.database;

import java.sql.SQLException;

import javax.naming.NamingException;

import main.java.com.virtualhand.server.objects.AppUser;

public interface AppUserManager extends DBManager<AppUser, Long> {

	AppUser getByUsername(String username) throws SQLException, NamingException;

}
