package main.java.com.virtualhand.server.database;

import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

public interface DBManager<T, PK> {
	
	List<T> getAll() throws SQLException, NamingException;
	
	List<T> get(PK id) throws SQLException, NamingException;
	
	T save(T obj) throws SQLException, NamingException;
	
	void remove(T obj) throws SQLException, NamingException;
	
	T update(T obj) throws SQLException, NamingException;
}
