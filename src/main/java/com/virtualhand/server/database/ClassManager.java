package main.java.com.virtualhand.server.database;

import java.sql.SQLException;

import javax.naming.NamingException;

import main.java.com.virtualhand.server.objects.Class;

public interface ClassManager extends DBManager<Class, Long> {
	Class getByClassNameAndSchool(String classname, String school) throws SQLException, NamingException;
}
