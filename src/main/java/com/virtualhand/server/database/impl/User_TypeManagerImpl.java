package main.java.com.virtualhand.server.database.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import main.java.com.virtualhand.server.database.User_TypeManager;
import main.java.com.virtualhand.server.objects.User_Type;

public class User_TypeManagerImpl implements User_TypeManager {

	private final String USER_TYPE_TABLE_NAME = "user_type";

	public User_TypeManagerImpl() throws NamingException, SQLException {
		// Connection result = null;
		// InitialContext ic = new InitialContext();
		// Context initialContext = (Context) ic.lookup("java:comp/env");
		// final DataSource datasource = (DataSource) initialContext
		// .lookup("jdbc/MySQLDS");
		// result = datasource.getConnection();
		// database = result.createStatement();
		// Thread thread = new Thread(){
		// @Override
		// public void run(){
		// long s = System.nanoTime();
		// Connection r;
		// while(true){
		// if(System.nanoTime() - s > 28800000000000l){
		// try {
		// r = datasource.getConnection();
		// database = r.createStatement();
		// s = System.nanoTime();
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// }
		// }
		// }
		// }
		// };
		// thread.start();
	}

	@Override
	public List<User_Type> getAll() throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		ResultSet classres = database.executeQuery("SELECT * FROM "
				+ this.USER_TYPE_TABLE_NAME + ";");

		ArrayList<User_Type> classes = new ArrayList<User_Type>();
		while (classres.next()) {
			User_Type cls = new User_Type(classres.getLong(1),
					classres.getString(2));
			classes.add(cls);
		}
		classres.close();
		result.close();
		return classes;
	}

	@Override
	public List<User_Type> get(Long id) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		ResultSet res = database.executeQuery("SELECT * FROM "
				+ this.USER_TYPE_TABLE_NAME + " WHERE user_id = " + id + ";");

		List<User_Type> list = new ArrayList<User_Type>();
		while (res.next()) {
			User_Type cls = new User_Type(res.getLong(1), res.getString(2));
			list.add(cls);
		}

		res.close();
		result.close();
		return list;
	}

	@Override
	public User_Type save(User_Type obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("INSERT INTO " + this.USER_TYPE_TABLE_NAME
				+ "(user_id, type) VALUES('" + obj.getUser_id() + "','"
				+ obj.getType() + "');");
		result.close();

		return obj;
	}

	@Override
	public void remove(User_Type obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("DELETE FROM " + this.USER_TYPE_TABLE_NAME
				+ " WHERE user_id=" + obj.getUser_id() + " AND type='"
				+ obj.getType() + "';");
		result.close();
	}

	@Override
	public User_Type update(User_Type obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("UPDATE " + this.USER_TYPE_TABLE_NAME
				+ " SET `user_id`=" + obj.getUser_id() + ", `type`='"
				+ obj.getType() + "'" + " WHERE `user_id`=" + obj.getUser_id()
				+ ";");
		result.close();
		return obj;
	}

}
