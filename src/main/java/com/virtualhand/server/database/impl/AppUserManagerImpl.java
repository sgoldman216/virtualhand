package main.java.com.virtualhand.server.database.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import main.java.com.virtualhand.server.database.AppUserManager;
import main.java.com.virtualhand.server.database.ClassManager;
import main.java.com.virtualhand.server.database.User_ClassManager;
import main.java.com.virtualhand.server.database.User_TypeManager;
import main.java.com.virtualhand.server.objects.AppUser;
import main.java.com.virtualhand.server.objects.User_Class;
import main.java.com.virtualhand.server.objects.User_Type;

import org.springframework.beans.factory.annotation.Autowired;

public class AppUserManagerImpl implements AppUserManager {

	private final String TABLE_NAME = "appuser";
	private final String TYPE_TABLE_NAME = "user_type";
	// private Statement database;
	// DataSource datasource;

	@Autowired
	ClassManager classMan;

	@Autowired
	User_ClassManager userClassMan;

	@Autowired
	User_TypeManager userTypeMan;

	public AppUserManagerImpl() throws NamingException, SQLException {
		// Connection result = null;
		// InitialContext ic = new InitialContext();
		// Context initialContext = (Context) ic.lookup("java:comp/env");
		// datasource = (DataSource) initialContext
		// .lookup("jdbc/MySQLDS");
		// result = datasource.getConnection();
		// database = result.createStatement();
	}

	@Override
	public List<AppUser> getAll() throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		ResultSet Userres = database.executeQuery("SELECT * FROM "
				+ this.TABLE_NAME + ";");
		ArrayList<AppUser> users = new ArrayList<AppUser>();
		while (Userres.next()) {
			AppUser user1 = new AppUser(Userres.getLong(1),
					Userres.getString(2), Userres.getString(3));
			users.add(user1);
		}
		Userres.close();
		result.close();
		for (int i = 0; i < users.size(); i++) {
			AppUser user = users.get(i);
			try {
				user.setUserType(userTypeMan.get(user.getId()).get(0).getType());
			} catch (IndexOutOfBoundsException e) {

			}
			// Logger.getGlobal().log(Level.INFO, "User: " +
			// user.getUsername());
			// ResultSet Typeres = database.executeQuery("SELECT * FROM "
			// + this.TYPE_TABLE_NAME + " WHERE user_id =" + user.getId()
			// + ";");
			// if (Typeres.next()) {
			// user.setUserType(Typeres.getString(2));
			// }
			// Typeres.close();
		}

		for (int i = 0; i < users.size(); i++) {
			AppUser user = users.get(i);
			user.setUser_Classes(userClassMan.get(user.getId()));
		}
		return users;
	}

	@Override
	public List<AppUser> get(Long id) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		ResultSet res = database.executeQuery("SELECT * FROM " + TABLE_NAME
				+ " WHERE id=" + id + ";");
		List<AppUser> list = new ArrayList<AppUser>();
		// while (res.next()) {
		res.next();
		AppUser user = new AppUser(res.getLong(1), res.getString(2),
				res.getString(3));
		res.close();
		ResultSet Typeres = database
				.executeQuery("SELECT * FROM " + this.TYPE_TABLE_NAME
						+ " WHERE user_id=" + user.getId() + ";");
		if (Typeres.next()) {
			user.setUserType(Typeres.getString(2));
		}
		Typeres.close();
		result.close();
		user.setUser_Classes(userClassMan.get(user.getId()));
		// ResultSet classres = database.executeQuery("SELECT * FROM "
		// + this.USER_CLASS_TABLE_NAME + " WHERE user_id=" + user.getId()
		// + ";");
		// while (classres.next()) {
		// user.addUser_Class(new User_Class(classres.getLong(1), classres
		// .getLong(2), classres.getInt(3)));
		// }
		// classres.close();
		list.add(user);
		// }
		return list;
	}

	@Override
	public AppUser save(AppUser obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("INSERT INTO " + TABLE_NAME
				+ "(username, password) VALUES('" + obj.getUsername() + "','"
				+ obj.getPassword() + "');");
		result.close();
		if (obj.getUserType() != null) {
			this.userTypeMan.save(new User_Type(this.getByUsername(
					obj.getUsername()).getId(), obj.getUserType()));
		}
		for (User_Class uc : obj.getUser_Classes()) {
			this.userClassMan.save(uc);
		}
		return obj;
	}

	@Override
	public void remove(AppUser obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("DELETE FROM " + this.TABLE_NAME + " WHERE id="
				+ obj.getId() + ";");
		result.close();
		for (User_Type ut : userTypeMan.get(obj.getId())) {
			userTypeMan.remove(ut);
		}

		for (User_Class ut : userClassMan.get(obj.getId())) {
			userClassMan.remove(ut);
		}
	}

	@Override
	public AppUser getByUsername(String username) throws SQLException,
			NamingException {

		List<AppUser> users = this.getAll();
		for (AppUser user : users) {
			if (user.getUsername().equals(username)) {
				return user;
			}
		}
		return null;
	}

	@Override
	public AppUser update(AppUser obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("UPDATE " + this.TABLE_NAME + " SET username='"
				+ obj.getUsername() + "', password='" + obj.getPassword()
				+ "' " + " WHERE id=" + obj.getId() + ";");
		result.close();
		User_Type type = this.userTypeMan.get(obj.getId()).get(0);
		type.setType(obj.getUserType());
		this.userTypeMan.update(type);

		List<User_Class> classes = userClassMan.get(obj.getId());
		outer: for (User_Class uc : classes) {
			for (User_Class id : obj.getUser_Classes()) {
				if (((Long) id.getUser_id()).equals(uc.getClass_id())) {
					continue outer;
				}
			}
			userClassMan.remove(uc);
		}

		classes = userClassMan.get(obj.getId());

		outer: for (User_Class id : obj.getUser_Classes()) {
			for (User_Class uc : classes) {
				if (id.equals(uc.getClass_id())) {
					continue outer;
				}
			}
			userClassMan.save(id);
		}
		return obj;
	}

}
