package main.java.com.virtualhand.server.database.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import main.java.com.virtualhand.server.database.KeyManager;
import main.java.com.virtualhand.server.objects.Key;

public class KeyManagerImpl implements KeyManager {

	private final String TABLE_NAME = "purchase_keys";

	@Override
	public List<Key> getAll() throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		ResultSet keyres = database.executeQuery("SELECT * FROM "
				+ this.TABLE_NAME + ";");

		ArrayList<Key> keys = new ArrayList<Key>();
		while (keyres.next()) {
			Key cls = new Key(keyres.getLong(1),
					keyres.getString(2), keyres.getInt(3));
			keys.add(cls);
		}
		keyres.close();
		result.close();
		return keys;
	}

	@Override
	public List<Key> get(Long id) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		ResultSet res = database.executeQuery("SELECT * FROM "
				+ this.TABLE_NAME + " WHERE id = " + id + ";");
		List<Key> list = new ArrayList<Key>();
		while (res.next()) {
			Key cls = new Key(res.getLong(1), res.getString(2),
					res.getInt(3));
			list.add(cls);
		}

		res.close();
		result.close();
		return list;
	}

	@Override
	public Key save(Key obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("INSERT INTO " + this.TABLE_NAME
				+ "(code, type) VALUES('" + obj.getKey() + "','"
				+ obj.getType() + "');");
		result.close();

		return obj;
	}

	@Override
	public void remove(Key obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("DELETE FROM " + this.TABLE_NAME + " WHERE id="
				+ obj.getId() + ";");
		result.close();
	}

	@Override
	public Key update(Key obj) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
