package main.java.com.virtualhand.server.database.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import main.java.com.virtualhand.server.database.ClassManager;
import main.java.com.virtualhand.server.objects.Class;

public class ClassManagerImpl implements ClassManager {

	private final String TABLE_NAME = "class";

	public ClassManagerImpl() throws NamingException, SQLException {
		// Connection result = null;
		// InitialContext ic = new InitialContext();
		// Context initialContext = (Context) ic.lookup("java:comp/env");
		// final DataSource datasource = (DataSource) initialContext
		// .lookup("jdbc/MySQLDS");
		// result = datasource.getConnection();
		// database = result.createStatement();
	}

	@Override
	public List<Class> getAll() throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		ResultSet classres = database.executeQuery("SELECT * FROM "
				+ this.TABLE_NAME + ";");

		ArrayList<Class> classes = new ArrayList<Class>();
		while (classres.next()) {
			Class cls = new Class(classres.getLong(1), classres.getString(2),
					classres.getString(3));
			classes.add(cls);
		}
		classres.close();
		result.close();
		return classes;
	}

	@Override
	public List<Class> get(Long id) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();

		ResultSet res = database.executeQuery("SELECT * FROM " + TABLE_NAME
				+ " WHERE id = " + id + ";");
		List<Class> list = new ArrayList<Class>();
		while (res.next()) {
			Class cls = new Class(res.getLong(1), res.getString(2),
					res.getString(3));
			list.add(cls);
		}
		res.close();
		result.close();
		return list;
	}

	@Override
	public Class save(Class obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("INSERT INTO " + TABLE_NAME
				+ "(className, school) VALUES('" + obj.getClassName() + "','"
				+ obj.getSchool() + "');");
		result.close();
		return obj;
	}

	@Override
	public void remove(Class obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("DELETE FROM " + this.TABLE_NAME + " WHERE id="
				+ obj.getId() + ";");
		result.close();
	}

	@Override
	public Class getByClassNameAndSchool(String classname, String school)
			throws SQLException, NamingException {
		List<Class> classes = this.getAll();
		for (Class cls : classes) {
			if (cls.getClassName().equals(classname)) {
				return cls;
			}
		}
		return null;
	}

	@Override
	public Class update(Class obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("UPDATE " + this.TABLE_NAME + " SET className='"
				+ obj.getClassName() + "', school='" + obj.getSchool() + "'"
				+ " WHERE id=" + obj.getId() + ";");
		result.close();
		return obj;
	}

}
