package main.java.com.virtualhand.server.database.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import main.java.com.virtualhand.server.database.User_ClassManager;
import main.java.com.virtualhand.server.objects.User_Class;

public class User_ClassManagerImpl implements User_ClassManager {

	private final String USER_CLASS_TABLE_NAME = "user_class";

	public User_ClassManagerImpl() throws NamingException, SQLException {
		// Connection result = null;
		// InitialContext ic = new InitialContext();
		// Context initialContext = (Context) ic.lookup("java:comp/env");
		// final DataSource datasource = (DataSource) initialContext
		// .lookup("jdbc/MySQLDS");
		// result = datasource.getConnection();
		// database = result.createStatement();

		// Thread thread = new Thread(){
		// @Override
		// public void run(){
		// long s = System.nanoTime();
		// Connection r;
		// while(true){
		// if(System.nanoTime() - s > 28800000000000l){
		// try {
		// r = datasource.getConnection();
		// database = r.createStatement();
		// s = System.nanoTime();
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// }
		// }
		// }
		// }
		// };
		// thread.start();
	}

	@Override
	public List<User_Class> getAll() throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		ResultSet classres = database.executeQuery("SELECT * FROM "
				+ this.USER_CLASS_TABLE_NAME + ";");

		ArrayList<User_Class> classes = new ArrayList<User_Class>();
		while (classres.next()) {
			User_Class cls = new User_Class(classres.getLong(1),
					classres.getLong(2), classres.getInt(3));
			cls.setDate(classres.getTimestamp(4));
			classes.add(cls);
		}
		classres.close();
		result.close();
		return classes;
	}

	@Override
	public List<User_Class> get(Long id) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		ResultSet res = database.executeQuery("SELECT * FROM "
				+ this.USER_CLASS_TABLE_NAME + " WHERE user_id = " + id + ";");
		List<User_Class> list = new ArrayList<User_Class>();
		while (res.next()) {
			User_Class cls = new User_Class(res.getLong(1), res.getLong(2),
					res.getInt(3));
			cls.setDate(res.getTimestamp(4));
			list.add(cls);
		}

		res.close();
		result.close();
		return list;
	}

	@Override
	public User_Class save(User_Class obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("INSERT INTO " + this.USER_CLASS_TABLE_NAME
				+ "(user_id, class_id, hand_state) VALUES('" + obj.getUser_id()
				+ "','" + obj.getClass_id() + "'," + obj.isHand() + ");");
		result.close();

		return obj;
	}

	@Override
	public void remove(User_Class obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("DELETE FROM " + this.USER_CLASS_TABLE_NAME
				+ " WHERE user_id=" + obj.getUser_id() + " AND class_id="
				+ obj.getClass_id() + ";");
		result.close();
	}

	@Override
	public User_Class update(User_Class obj) throws SQLException,
			NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("UPDATE " + this.USER_CLASS_TABLE_NAME
				+ " SET user_id=" + obj.getUser_id() + ", class_id='"
				+ obj.getClass_id() + ", hand_state=" + obj.isHand() + ""
				+ " WHERE user_id=" + obj.getUser_id() + " AND class_id='"
				+ obj.getClass_id() + "';");
		result.close();
		return obj;

	}

}
