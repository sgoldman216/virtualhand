package main.java.com.virtualhand.server.database.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import main.java.com.virtualhand.server.database.LogManager;
import main.java.com.virtualhand.server.objects.Log;

public class LogManagerImpl implements LogManager {

	private final String TABLE_NAME = "data_logs";

	@Override
	public List<Log> getAll() throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		ResultSet logres = database.executeQuery("SELECT * FROM "
				+ this.TABLE_NAME + ";");

		ArrayList<Log> keys = new ArrayList<Log>();
		while (logres.next()) {
			Log log = new Log(logres.getLong("id"), logres.getTimestamp("timestamp"), logres.getString("type"), logres.getString("location"), logres.getString("content"));
			keys.add(log);
		}
		logres.close();
		result.close();
		return keys;
	}

	@Override
	public List<Log> get(Long id) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Log save(Log obj) throws SQLException, NamingException {
		Statement database;
		Connection result = null;
		InitialContext ic = new InitialContext();
		Context initialContext = (Context) ic.lookup("java:comp/env");
		final DataSource datasource = (DataSource) initialContext
				.lookup("jdbc/MySQLDS");
		result = datasource.getConnection();
		database = result.createStatement();
		database.execute("INSERT INTO " + this.TABLE_NAME
				+ "(type, location, content) VALUES('" + obj.getType() + "','"
				+ obj.getLocation() + "','" + obj.getMessage() + "');");
		result.close();

		return obj;
	}

	@Override
	public void remove(Log obj) throws SQLException, NamingException {
		// TODO Auto-generated method stub

	}

	@Override
	public Log update(Log obj) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
