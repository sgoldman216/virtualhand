package main.java.com.virtualhand.server.authentication;

import java.sql.SQLException;

import javax.naming.NamingException;

import main.java.com.virtualhand.server.objects.AppUser;

public interface AuthenticationManager
{
    /**
     * Returns the logged-in {@link org.cropster.dash.model.User}.
     * @return the user object or null
     * @throws NamingException 
     * @throws SQLException 
     */
    AppUser getAuthenticatedUser() throws NamingException, SQLException;

    /**
     * Returns the username of the logged-in
     * {@link org.cropster.dash.model.User}.
     * @return the username or null
     * @throws NamingException 
     * @throws SQLException 
     */
    String getAuthenticatedUsername() throws SQLException, NamingException;
}
