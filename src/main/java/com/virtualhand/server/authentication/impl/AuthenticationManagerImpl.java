package main.java.com.virtualhand.server.authentication.impl;

import java.sql.SQLException;

import javax.naming.NamingException;

import main.java.com.virtualhand.server.authentication.AuthenticationManager;
import main.java.com.virtualhand.server.database.AppUserManager;
import main.java.com.virtualhand.server.database.LogManager;
import main.java.com.virtualhand.server.objects.AppUser;
import main.java.com.virtualhand.server.objects.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class AuthenticationManagerImpl implements AuthenticationManager,
		UserDetailsService {

	private AppUserManager appUserManager;
	@Autowired
	private LogManager logManager;

	public void setAppUserManager(AppUserManager appUserManager) {
		this.appUserManager = appUserManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		AppUser user = null;
		try {
			user = appUserManager.getByUsername(username);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (null == user) {
			throw new UsernameNotFoundException("The user with name "
					+ username + " was not found");
		}

		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.cropster.dash.services.impl.AuthenticationManager#getAuthenticatedUser
	 * ()
	 */
	public AppUser getAuthenticatedUser() throws NamingException, SQLException {
		String username = getAuthenticatedUsername();
		if (username == null) {
			return null;
		}

		return appUserManager.getByUsername(username);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cropster.dash.services.impl.AuthenticationManager#
	 * getAuthenticatedUsername()
	 */
	public String getAuthenticatedUsername() throws SQLException, NamingException {
		return getUserDetails() != null ? getUserDetails().getUsername() : null;
	}

	private UserDetails getUserDetails() throws SQLException, NamingException {
		if (SecurityContextHolder.getContext() == null
				|| SecurityContextHolder.getContext().getAuthentication() == null) {
			return null;
		}
		
		//logManager.save(new Log(-1, null, "info", "server", "INFO: Server: AuthenticationManagerImpl: getAuthenticatedUsername(): " + SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString()));

		return (UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
	}
}
