package main.resources;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

@SuppressWarnings("unused")
public class Test {

	public static void main(String[] args) {
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("me", "42");
		Client student = ClientBuilder.newClient();
		student.register(feature);
		
		HttpAuthenticationFeature feature1 = HttpAuthenticationFeature.basic("McD", "McD");
		Client mcd = ClientBuilder.newClient();
		mcd.register(feature1);
		
		HttpAuthenticationFeature feature2 = HttpAuthenticationFeature.basic("Gregg", "Gregg");
		Client gregg = ClientBuilder.newClient();
		gregg.register(feature2);
		
		HttpAuthenticationFeature feature3 = HttpAuthenticationFeature.basic("Olriechs", "Olriechs");
		Client olr = ClientBuilder.newClient();
		olr.register(feature3);
		
		HttpAuthenticationFeature feature4 = HttpAuthenticationFeature.basic("sam", "sam");
		Client sam = ClientBuilder.newClient();
		sam.register(feature4);
		
		HttpAuthenticationFeature feature5 = HttpAuthenticationFeature.basic("talya", "talya");
		Client tal = ClientBuilder.newClient();
		tal.register(feature5);
		
		HttpAuthenticationFeature feature6 = HttpAuthenticationFeature.basic("rami", "rami");
		Client rami = ClientBuilder.newClient();
		rami.register(feature6);
		
		HttpAuthenticationFeature feature7 = HttpAuthenticationFeature.basic("Gregg", "gregggregg");
		Client gregg2 = ClientBuilder.newClient();
		gregg2.register(feature7);
		
		Client registerer = ClientBuilder.newClient();
//		Test.testRegisterTeacher("McD", "McD", "APP", "OHS", registerer);
//		Test.testRegisterTeacher("Gregg", "Gregg", "TECH", "OHS", registerer);
//		Test.testRegisterTeacher("Olriechs", "Olriechs", "Calc", "OHS", registerer);
//		Test.testRegisterStudent("sam", "sam", 1002, registerer);
//		Test.testRegisterStudent("talya", "talya", 1003, registerer);
//		Test.testRegisterStudent("rami", "rami", 1004, registerer);
		
		Test.testSetHandStatus(1001, 2, student);
		
		//System.out.println(Test.testAddClass(1003, sam));
		//System.out.println(Test.testAddClass(1004, sam));
		//System.out.println(Test.testAddClass(1004, tal));
		//System.out.println(Test.testSetHandStatus(1003, 2, sam));
		//System.out.println(Test.testTeacherAddNewClass("ALG", "OHS", mcd));
		//System.out.println(Test.testTeacherGetClasses(mcd));
		//System.out.println(Test.testTeacherSetHandStatus(10, 1002, 2, mcd));
		//System.out.println(Test.testTeacherGetHandsForClass(1003, gregg));
		//System.out.println(Test.testTeacherGetHandsForClass(1004, gregg));
		//System.out.println(Test.testTeacherChangePassword("gregggregg", gregg));
		//System.out.println(Test.testTeacherGetHandsForClass(1003, gregg));
		//System.out.println(Test.testTeacherGetHandsForClass(1003, gregg2));
		//System.out.println(Test.testTeacherGetStudents(1004, olr));
		//System.out.println(Test.testRemoveStudent(12, 1004, olr));
		//System.out.println(Test.testTeacherGetStudents(1004, olr));
		//System.out.println(Test.testTeacherRemoveClass(1002, mcd));
		//System.out.println(Test.testTeacherRemoveClass(1003, gregg2));
		//System.out.println(Test.testTeacherRemoveClass(1004, olr));
		//System.out.println(Test.testTeacherRemoveClass(1007, mcd));
	}
	
	private static String testRegisterStudent(String username, String password, int firstClass, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/student/register");
		 
		Form form = new Form();
		form.param("username", username);
		form.param("password", password);
		form.param("firstclass", "" + firstClass);
		
		String ret = target.request()
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testRegisterTeacher(String username, String password, String classname, String school, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/teacher/register");
		 
		Form form = new Form();
		form.param("username", username);
		form.param("password", password);
		form.param("classname", classname);
		form.param("school", school);
		
		String ret = target.request()
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testGetHandStatus(int cls, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/student/getHandStatus");
		 
		Form form = new Form();
		form.param("class_id", "" + cls);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}

	private static String testSetHandStatus(int cls, int state, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/student/setHandStatus");
		 
		Form form = new Form();
		form.param("class_id", "" + cls);
		form.param("handstate", "" + state);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testAddClass(int cls, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/student/addClass");
		 
		Form form = new Form();
		form.param("class_id", "" + cls);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testChangePassword(String pass, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/student/changePassword");
		 
		Form form = new Form();
		form.param("password", pass);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testGetClasses(Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/student/getClasses");
		 
		Form form = new Form();
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testTeacherSetHandStatus(int id, int cls, int handstate, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/teacher/setHandStatus");
		 
		Form form = new Form();
		form.param("id", "" + id);
		form.param("class_id", "" + cls);
		form.param("handstate", "" + handstate);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testTeacherAddNewClass(String classname, String school, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/teacher/addNewClass");
		 
		Form form = new Form();
		form.param("classname", classname);
		form.param("school", school);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testTeacherChangePassword(String pass, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/teacher/changePassword");
		 
		Form form = new Form();
		form.param("password", pass);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testTeacherGetClasses(Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/teacher/getClasses");
		 
		Form form = new Form();
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testTeacherGetHandsForClass(int cls, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/teacher/getHandsForClass");
		 
		Form form = new Form();
		form.param("class_id", "" + cls);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testTeacherGetStudents(int cls, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/teacher/getStudents");
		 
		Form form = new Form();
		form.param("class_id", "" + cls);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testTeacherRemoveClass(int cls, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/teacher/removeClass");
		 
		Form form = new Form();
		form.param("class_id", "" + cls);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
	
	private static String testRemoveStudent(int id, int cls, Client client){
		WebTarget target = client.target("http://api-virtualhand.rhcloud.com").path("api/v1/teacher/removeStudent");
		 
		Form form = new Form();
		form.param("id", "" + id);
		form.param("class_id", "" + cls);
		
		String ret = target.request(MediaType.APPLICATION_JSON)
		    .post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED)).readEntity(String.class);
		
		return ret;
	}
}
